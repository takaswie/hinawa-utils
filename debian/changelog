hinawa-utils (0.3.0-3) unstable; urgency=medium

  * debian/control
    - Add d/clean to fix FTBFS (Closes: #1049175)
    - Bump Standards-Version to 4.6.2. No other changes are required.

 -- Kentaro Hayashi <kenhys@xdump.org>  Sat, 19 Aug 2023 21:29:51 +0900

hinawa-utils (0.3.0-2) unstable; urgency=medium

  * debian/control
    - Bump standards-version to 4.6.0. No other changes are required.
  * debian/watch
    - Fix uscan error
  * debian/hinawa-utils.lintian-overrides
    - Fix mismatched-override pattern

 -- Kentaro Hayashi <kenhys@xdump.org>  Sun, 20 Nov 2022 17:33:44 +0900

hinawa-utils (0.3.0-1) unstable; urgency=medium

  * New upstream version 0.3.0
  * Upload libhinawa 2.x supported version (Closes: #971908)
  * debian/control
    - Bump standards-version to 4.6.0. No other changes are required.
  * debian/hinawa-utils.install
    - Update supported cli files
  * Drop needless debian/patches/use-hinawa-3.0.patch

 -- Kentaro Hayashi <kenhys@xdump.org>  Thu, 24 Mar 2022 20:33:29 +0900

hinawa-utils (0.2.0-3) unstable; urgency=low

  * debian/upstream/metadata
    - Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
      Repository-Browse.
  * debian/control
    - Follow dependency to gir1.2-hinawa-3.0.
  * debian/patches/use-hinawa-3.0.patch
    - Use gi.require_version('Hinawa', '3.0') (Closes: #968026)

 -- Kentaro Hayashi <kenhys@xdump.org>  Sat, 12 Sep 2020 17:55:07 +0900

hinawa-utils (0.2.0-2) unstable; urgency=medium

  * debian/control
    - Bump debhelper version to 13
    - Bump standard-version to 4.5.0. No other changes are required.
  * debian/hinawa-utils.install
    - Install missing cli files

 -- Kentaro Hayashi <kenhys@xdump.org>  Fri, 15 May 2020 16:43:41 +0900

hinawa-utils (0.2.0-1) unstable; urgency=medium

  * New upstream version 0.2.0
  * debian/NEWS
    - Add note about executable file names in the latest release.
  * debian/control
    - Bump standard-version to 4.4.1. No other changes are required.
    - Add Rules-Requires-Root: no
    - Use debhelper-compat instead of d/compat.
  * debian/hinawa-utils.lintian-overrides
    - Update list of command line executable file names.
  * debian/hinawa-utils.install
    - Update list of command line executable file names.

 -- Kentaro Hayashi <hayashi@clear-code.com>  Fri, 17 Jan 2020 21:36:24 +0900

hinawa-utils (0.1.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No-change source-only upload to allow testing migration.

 -- Boyuan Yang <byang@debian.org>  Fri, 11 Oct 2019 14:05:06 -0400

hinawa-utils (0.1.0-2) unstable; urgency=medium

  * debian/control
    - Split command-line tools into hinawa-utils package (Closes: #913804)
    - Bump standard-version to 4.3.0. No other changes are required.
  * debian/hinawa-utils.lintian-overrides
    - Suppress warning about manpages.

 -- Kentaro Hayashi <hayashi@clear-code.com>  Sun, 27 Jan 2019 15:13:27 +0900

hinawa-utils (0.1.0-1) unstable; urgency=medium

  * Initial release (Closes: #905840)

 -- Kentaro Hayashi <hayashi@clear-code.com>  Fri, 07 Sep 2018 17:15:28 +0900
